<!-- heading -->
## KiCAD Footprints and PCB notes
* Assign foot prints by clicking on "Tools" in schematic layout.
  
* To assign foot prints for each components select types of components in use.
* Apply, save schematic and continue.
* From the schematic page open PCB in the doard editor.
* To import the schematic drawing in the doard editor click on update PCB with changes made to schematic(F8).
* Use traces to give connection (X).
* Select the required layer and disable the ones not required.
* To mark the cutting edge of the PCB use table.
* Use design rule checker to check the connection.
  
## PCB Design

![PCB 2D](image/PCB%202D.png)

