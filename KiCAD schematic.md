<!-- Heading -->
# KiCAD Schematic Notes
## Steps

1. Creat a new project.
2. In the blank layout page insert the components. 
### Components I have used are:
<!-- UL -->
* Resistor
* LED
* Connector
* Ground
* Voltage common collector
* Wire
3. Join all the components.
4. Annotate the text.
5. Check the circuit using "Electrical rule checker".

<!-- Italics -->
*To load components use key "A"*


## Kicad schematic drawing

![KiCAD sche](image/KiCAD%20Sche.png)




   


